%global debug_package %{nil}

Name:           opkg-utils
Version:        0.6.3
Release:        1
Summary:        Helper scripts for use with the opkg package manager

License:        GPLv2+
URL:            https://git.yoctoproject.org/opkg-utils
Source0:        https://git.yoctoproject.org/opkg-utils/snapshot/%{name}-%{version}.tar.gz

BuildRequires:  make
BuildRequires:  perl-podlators
Requires:       bash
Requires:       coreutils
Requires:       python3
Requires:       python3-pydot

%description
Helper scripts for use with the opkg package manager.


%prep
%autosetup


%build
make


%install
rm -rf $RPM_BUILD_ROOT
%makeinstall


%files
%{_bindir}/*
%license COPYING
%{_mandir}/man1/*


%changelog
* Fri Apr 19 2024 yueyuankun <yueyuankun@kylinos.cn> - 0.6.3-1
- Update package to version 0.6.3
- CONTRIBUTING: refresh document
- opkg-feed: Fix adding feeds with same name as architecture
- and so on

* Fri Jan 14 2022 Lv Genggeng <lvgenggeng@uniontech.com> - 0.5.0-1
- Package init
